# TASK MANAGER

## DEVELOPER INFO

**NAME**: Olga Nikitushkina

**E-MAIL**: onikitushkina@t1-consulting.ru

## SOFTWARE

**OS**: Windows 10

**JDK**: OPENJDK 1.8.0_322

## HARDWARE

**CPU**: i5

**RAM**: 17GB

**SSD**: 512GB

## RUN PROGRAM

```
java -jar task-manager.jar
```
