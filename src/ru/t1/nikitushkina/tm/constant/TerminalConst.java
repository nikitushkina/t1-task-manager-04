package ru.t1.nikitushkina.tm.constant;

public final class TerminalConst {

    public static final String HELP = "help";

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    private TerminalConst() {
    }

}
